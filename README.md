Dotfiles for Local Environment Setups
=====================================

See dotfiles in `home` directory for local config files.

Use symlinks for easier updates later on. E.g.:

    ln -s dotfiles/.bashrc


Terminal
--------

* git clone https://github.com/Anthony25/gnome-terminal-colors-solarized.git
* cd gnome-terminal-colors-solarized
* ./install.sh


Sublime Text Editor
-------------------

* Install package manager:

        import urllib.request,os,hashlib; h = 'eb2297e1a458f27d836c04bb0cbaf282' + 'd0e7a3098092775ccb37ca9d6b2e4b7d'; pf = 'Package Control.sublime-package'; ipp = sublime.installed_packages_path(); urllib.request.install_opener( urllib.request.build_opener( urllib.request.ProxyHandler()) ); by = urllib.request.urlopen( 'http://packagecontrol.io/' + pf.replace(' ', '%20')).read(); dh = hashlib.sha256(by).hexdigest(); print('Error validating download (got %s instead of %s), please try manual install' % (dh, h)) if dh != h else open(os.path.join( ipp, pf), 'wb' ).write(by)

* Install:

	* `solarized theme`
	* `solarized toggle`
	* `less`
	* `markdown preview`
	* `terminal`: launch terminals from current directory
